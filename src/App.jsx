import { useState } from 'react'
import Button from './components/button'
import './App.css'

function App() {
  const [count, setCount] = useState(0) /* variable nommée "count" définie avec la valeur de départ 0 grâce à la fonction useState() , setCount est une fonction à laquelle je vais pouvoir passer une action à réaliser*/


  const message = count > 0 ? `Nombre de participants : ${count}` : "Aucun participant";



  return (
    <>
      <div className="card">
        <p>
          {message}
        </p>

        <Button action={() => setCount((count) => count + 1)} label={"Ajouter un participant"} /> { /* j'incrémente de 1 à chaque click */}

        <Button action={() => setCount((count) => (count < 0 ? 0 : count - 1))} label={"Enlever un participant"} /> { /* je décrémente de 1 à chaque click */}

        <Button action={() => setCount(0)} label={"Revenir à 0"} /> { /* En cliquant sur ce bouton je redonne la valeur 0 à count*/}

      </div>
    </>
  )
}

export default App
